
# Bases de datos Avanzadas
#### Por Erick de Jesus Hernandez Cerecedo

El siguiente repositorio contiene un la actividad final parcial de la clases bases de datos avanzadas.

## Consultas dentro de la actividad

**1. Consulta Numero Uno**\
Query the list of available plants for rental in the plant catalog using the plant name (a substring of it) and the rental period. Note that the query must return the plant information (i.e. `PlantInventoryEntry`) and the number of serviceable items available (i.e. the count of `PlantInventoryItem` s)

```java
// Query's correspondientes a la primera consulta:
@Query("SELECT p.plant.id FROM PlantReservation AS p WHERE p.rentalPeriod.startDate = ?1 AND p.rentalPeriod.endDate = ?2")
public List<Long> findPlantIdByStartAndEndDate(LocalDate startDate, LocalDate endDate);

@Query("SELECT p.plant FROM PlantReservation  AS p WHERE p.rentalPeriod.startDate = ?1 AND p.rentalPeriod.endDate = ?2 AND p.plant.equipmentCondition = 'SERVICEABLE'")
public List<PlantInventoryItem> findPlantInventoryItemBySEDateWhereServiceable(LocalDate startDate, LocalDate endDate);

@Query("SELECT p.plantInfo FROM PlantInventoryItem AS p Where p.id IN ?1 AND name LIKE %?2%")
List<PlantInventoryEntry> findPlantInventoryEntryByIDAndNameLike(List<Long> idList, String name);

```

```java
// Query Test correspondientes a la primera consulta:
@Test  
public void queryOne(){  
  //Parameters  
  String name= "Mini";  
  LocalDate startDate = LocalDate.of(2017, 03, 22);  
  LocalDate endDate = LocalDate.of(2017, 03, 24);  

  //Queries  
  List<Long> plantINventoryItemIds = plantReservationRepository.findPlantIdByStartAndEndDate(startDate, endDate);  
  List<PlantInventoryItem> plantInventoryItems = plantReservationRepository.findPlantInventoryItemBySEDateWhereServiceable(startDate, endDate);  
  List<PlantInventoryEntry> plantInventoryEntries = plantInventoryItemRepository.findPlantInventoryEntryByIDAndNameLike(plantINventoryItemIds, name);  

  System.out.println("Plant Inventory Items " + plantInventoryEntries);  
  System.out.printf("\nServiceable Plant Inventory Items: #%s   ", plantInventoryItems.size());  

  //Assertion  
  assertThat(plantInventoryEntries.size()).isEqualTo(1);  

}

```

**2. Consulta Numero Dos**\
	Check there is at least one item for a given entry available for rental in a given period of time

-   Strict version: The plant is `serviceable` at the moment of the query

-   Relaxed version: The plant is `serviceable` at the moment of the query or the requested period is at least three weeks later in the future and the plant is programed for maintenance at least one week before the rental

```java
// Query's correspondientes a la segunda consulta:
@Query("SELECT p.plant.id FROM PlantReservation AS p WHERE p.rentalPeriod.startDate = ?1 AND p.rentalPeriod.endDate = ?2")  
public List<Long> findPlantInventoryEntryIdWhereUnavailable(LocalDate targetStart, LocalDate targetEnd);

@Query("SELECT p.id FROM PlantInventoryItem  AS p")  
public List<Long> findAllIds();  

@Query("SELECT p FROM PlantInventoryItem AS p WHERE p.id IN ?1 AND NOT p.id IN ?2 AND equipmentCondition = 'SERVICEABLE'")  
List<PlantInventoryItem> findAllByIdWhereServiceable(List<Long> idYes, List<Long> idNo);

```

```java
// Query Test correspondientes a la segunda consulta:
@Test  
public void queryTwo(){  

    //Parameters  
  LocalDate targetStart = LocalDate.of(2017, 03,22);  
  LocalDate targetEnd = LocalDate.of(2017,03,24);  


  //queries  
  List<Long> plantInventoryItemUnavailableIds = plantReservationRepository.findPlantInventoryEntryIdWhereUnavailable(targetStart, targetEnd);  
  List<Long> plantInventoryItemAvailable  = plantInventoryItemRepository.findAllIds();  
  List<PlantInventoryItem> plantInventoryItems = plantInventoryItemRepository.findAllByIdWhereServiceable(plantInventoryItemAvailable, plantInventoryItemUnavailableIds);  

  //Output  
  System.out.printf("Available: %s \n", plantInventoryItems);  
  System.out.printf("Serviceable Plant Inventory Items: %s\n " ,plantInventoryItems.size());  

  //Assertion  
  assertThat(plantInventoryItems.size()).isEqualTo(1);  
}

```

**3. Consulta Numero Tres**\
	Query the list of plants that have not been hired during the last 6 months
```java
// Query's correspondientes a la tercera consulta:
@Query("SELECT p.id FROM PlantInventoryEntry AS p")  
public List<Long> findAllIds();

@Query("SELECT p.plant.plantInfo.id FROM PlantReservation AS p WHERE p.rentalPeriod.startDate > ?1")  
public List<Long> findPlantsUsedInPastSixMonths(LocalDate sixMonthsBefore);

@Query("SELECT p FROM PlantInventoryEntry AS p WHERE p.id IN ?1 AND NOT p.id IN ?2")  
public List<PlantInventoryEntry> findByYesNoIds(List<Long> idYes, List<Long> idNo);

```

```java

// Query Test correspondientes a la tercera consulta:
@Test  
public void queryThree(){  

    // Parameters  
  LocalDate test = LocalDate.of(2017,02,10); // Se simula que es el 2017.  

 // Queries  List<Long> allEntries = plantInventoryEntryRepository.findAllIds(); // Todas las Entries Menos -v  
  List<Long> entriesUsed = plantReservationRepository.findPlantsUsedInPastSixMonths(test); // Las Entries que se usaron el periodo.  
  List<PlantInventoryEntry> result = plantInventoryEntryRepository.findByYesNoIds(allEntries, entriesUsed); // Entries de Resultado  

 // Output  System.out.printf("PlantInventoryEntries: %s\n", result);  

  // Assertion  
  assertThat(result.size()).isEqualTo(13);  
}

```

**4. Consulta Numero Cuatro**\
	Compute the number of corrective repairs per year for the last 5 years (the query result must include the year and the count of repairs in that year)

```java
// Query's correspondientes a la cuarta consulta:
@Query("SELECT new com.example.demo.model.Pair(COUNT(p), SUBSTRING(p.rentalPeriod.startDate, 1, 4)) FROM MaintenanceTask AS p WHERE p.typeOfWork = 'CORRECTIVE' AND p.rentalPeriod.startDate > ?1 AND p.rentalPeriod.endDate < ?2 GROUP BY SUBSTRING(p.rentalPeriod.startDate, 1, 4)")  
List<Pair> findRepairsByYear(LocalDate targetStart, LocalDate targetEnd);  

```

```java

// Query Test correspondientes a la cuarta consulta:
@Test  
public void queryFour() {  

    // Parameters  
  LocalDate targetEnd = LocalDate.now();  
  LocalDate targetStart = targetEnd.minusYears(5);  

  // Queries  
  List<Pair> result = maintenanceTaskRepository.findRepairsByYear(targetStart, targetEnd);  

  // Output  
  result.stream().forEach(System.out::println);  

}

```

**5. Consulta Numero Cinco**\
	Compute the cost due to corrective repairs per year for the last 5 years (the query result must include the year and the cost associated with that year)

```java
// Query's correspondientes a la quinta consulta:
@Query("SELECT new com.example.demo.model.Pair(SUM(p.price), SUBSTRING(p.rentalPeriod.startDate, 1, 4)) FROM MaintenanceTask AS p WHERE p.typeOfWork = 'CORRECTIVE' AND p.rentalPeriod.startDate > ?1 AND p.rentalPeriod.endDate < ?2 GROUP BY SUBSTRING(p.rentalPeriod.startDate, 1, 4)")  
List<Pair> findRepairsByPrice(LocalDate targetStart, LocalDate targetEnd);

```

```java

// Query Test correspondientes a la quinta consulta:
@Test  
public void queryFive(){  
    LocalDate targetEnd = LocalDate.now();  
  LocalDate targetStart = targetEnd.minusYears(5);  

  List<Pair> result = maintenanceTaskRepository.findRepairsByPrice(targetStart, targetEnd);  

  result.stream().forEach(System.out::println);  
}

```
### Datos
>Alumno y Matricula:
>Erick de Jesus Hernández Cerecedo
>A01066428
>Practia Parcial Bases de Datos Avanzadas
