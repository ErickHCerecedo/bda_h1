package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class PlantReservation {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    PurchaseOrder rental;
    @OneToOne
    PlantInventoryItem plant;
    @OneToOne
    MaintenancePlan maintPlan;

    @Embedded
    BusinessPeriod rentalPeriod;
}
