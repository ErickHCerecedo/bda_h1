package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class MaintenanceTask {

    @Id
    @GeneratedValue
    Long id;

    String description;

    @Enumerated(EnumType.STRING)
    TypeOfWork typeOfWork;

    @Column(precision=8,scale=2)
    BigDecimal price;

    @Embedded
    BusinessPeriod rentalPeriod;

    @OneToOne
    PlantReservation reservation;
}
