package com.example.demo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {

    // Query Uno
    @Query("SELECT p.plantInfo FROM PlantInventoryItem AS p Where p.id IN ?1 AND name LIKE %?2%")
    List<PlantInventoryEntry> findPlantInventoryEntryByIDAndNameLike(List<Long> idList, String name);

    // Query Dos
    @Query("SELECT p.id FROM PlantInventoryItem  AS p")
    public List<Long> findAllIds();

    @Query("SELECT p FROM PlantInventoryItem AS p WHERE p.id IN ?1 AND NOT p.id IN ?2 AND equipmentCondition = 'SERVICEABLE'")
    List<PlantInventoryItem> findAllByIdWhereServiceable(List<Long> idYes, List<Long> idNo);

}