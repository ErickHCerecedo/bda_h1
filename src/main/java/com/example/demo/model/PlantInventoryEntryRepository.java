package com.example.demo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long>, AdvancedMethodsForPlantInventoryEntry {

    // Query tres
    @Query("SELECT p.id FROM PlantInventoryEntry AS p")
    public List<Long> findAllIds();

    @Query("SELECT p FROM PlantInventoryEntry AS p WHERE p.id IN ?1 AND NOT p.id IN ?2")
    public List<PlantInventoryEntry> findByYesNoIds(List<Long> idYes, List<Long> idNo);


    @Query("SELECT p FROM PlantInventoryEntry AS p")
    public List<PlantInventoryEntry> findAll();

    @Query("SELECT p FROM PlantInventoryEntry AS p WHERE p.name LIKE %?1%")
    public List<PlantInventoryEntry> findByNameContaining(String name);

    @Query("SELECT p FROM PlantInventoryEntry AS p WHERE p.price = ?1")
    public List<PlantInventoryEntry> findSomething(BigDecimal precio);

}