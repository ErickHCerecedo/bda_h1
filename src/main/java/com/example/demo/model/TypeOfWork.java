package com.example.demo.model;

public enum TypeOfWork {
    PREVENTIVE, CORRECTIVE, OPERATIVE
}
