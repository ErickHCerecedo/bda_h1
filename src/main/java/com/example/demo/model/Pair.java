package com.example.demo.model;

import lombok.Data;

@Data
public class Pair {

    Object first;
    Object second;

    public Pair(Object first, Object second){
        this.first = first;
        this.second = second;
    }
}
