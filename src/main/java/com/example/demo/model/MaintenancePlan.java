package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class MaintenancePlan {

    @Id
    @GeneratedValue
    Long id;

    int yearOfAction;

    @OneToOne
    PlantInventoryItem plant;
    @OneToMany(cascade={CascadeType.ALL})
    List<MaintenanceTask> tasks;
}
