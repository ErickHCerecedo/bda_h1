package com.example.demo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MaintenanceTaskRepository extends JpaRepository<MaintenanceTask, Long> {
/*
    // COUNT(p) AS repairs, p.rentalPeriod.startDate
    @Query("SELECT COUNT(p) AS repairs, p.rentalPeriod.startDate FROM MaintenanceTask AS p WHERE p.typeOfWork = 'CORRECTIVE' AND p.rentalPeriod.startDate > ?1 AND p.rentalPeriod.endDate < ?2 GROUP BY p.rentalPeriod.startDate")
    public List<Object[]> findRepairsByYear(LocalDate targetStart, LocalDate targetEnd);
*/

    // Query cuatro
    @Query("SELECT new com.example.demo.model.Pair(SUBSTRING(p.rentalPeriod.startDate, 1, 4), COUNT(p)) FROM MaintenanceTask AS p WHERE p.typeOfWork = 'CORRECTIVE' AND p.rentalPeriod.startDate > ?1 AND p.rentalPeriod.endDate < ?2 GROUP BY SUBSTRING(p.rentalPeriod.startDate, 1, 4)")
    List<Pair> findRepairsByYear(LocalDate targetStart, LocalDate targetEnd);

    // Query cuatro
    @Query("SELECT new com.example.demo.model.Pair(SUM(p.price), SUBSTRING(p.rentalPeriod.startDate, 1, 4)) FROM MaintenanceTask AS p WHERE p.typeOfWork = 'CORRECTIVE' AND p.rentalPeriod.startDate > ?1 AND p.rentalPeriod.endDate < ?2 GROUP BY SUBSTRING(p.rentalPeriod.startDate, 1, 4)")
    List<Pair> findRepairsByPrice(LocalDate targetStart, LocalDate targetEnd);

/*
    @Query("SELECT COUNT(p.typeOfWork) FROM MaintenanceTask AS p WHERE typeOfWork = 'CORRECTIVE' AND p.rentalPeriod.endDate > ?1")
    public List<Integer> findRepairsByYear(LocalDate fiveYearsBefore);
*/

}
