package com.example.demo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlantReservationRepository extends JpaRepository<PlantReservation, Long>, AdvancedMethodsForPlantInventoryEntry  {

    // Query Uno
    @Query("SELECT p.plant.id FROM PlantReservation AS p WHERE p.rentalPeriod.startDate = ?1 AND p.rentalPeriod.endDate = ?2")
    public List<Long> findPlantIdByStartAndEndDate(LocalDate startDate, LocalDate endDate);

    @Query("SELECT p.plant FROM PlantReservation  AS p WHERE p.rentalPeriod.startDate = ?1 AND p.rentalPeriod.endDate = ?2 AND p.plant.equipmentCondition = 'SERVICEABLE'")
    public List<PlantInventoryItem> findPlantInventoryItemBySEDateWhereServiceable(LocalDate startDate, LocalDate endDate);

    // Query Dos
    @Query("SELECT p.plant.id FROM PlantReservation AS p WHERE p.rentalPeriod.startDate = ?1 AND p.rentalPeriod.endDate = ?2")
    public List<Long> findPlantInventoryEntryIdWhereUnavailable(LocalDate targetStart, LocalDate targetEnd);

    // Query tres
    @Query("SELECT p.plant.plantInfo.id FROM PlantReservation AS p WHERE p.rentalPeriod.startDate > ?1")
    public List<Long> findPlantsUsedInPastSixMonths(LocalDate sixMonthsBefore);

}
