package com.example.demo;

import com.example.demo.model.PlantInventoryEntryRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.math.BigDecimal;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);

		PlantInventoryEntryRepository repo = ctx.getBean(PlantInventoryEntryRepository.class);
		System.out.println(repo.findSomething(BigDecimal.valueOf(200)));

	}

}
