package com.example.demo.model;

import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@Sql(scripts="plants-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

public class InventoryRepositoryTest {

    // Links
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;
    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepository;


    @Test
    public void queryOne(){
        //Parameters
        String name= "Mini";
        LocalDate startDate = LocalDate.of(2017, 03, 22);
        LocalDate endDate = LocalDate.of(2017, 03, 24);

        //Queries
        List<Long> plantINventoryItemIds = plantReservationRepository.findPlantIdByStartAndEndDate(startDate, endDate);
        List<PlantInventoryItem> plantInventoryItems = plantReservationRepository.findPlantInventoryItemBySEDateWhereServiceable(startDate, endDate);
        List<PlantInventoryEntry> plantInventoryEntries = plantInventoryItemRepository.findPlantInventoryEntryByIDAndNameLike(plantINventoryItemIds, name);

        // Output
        System.out.printf("\nFirst Consultation: \n\nThe list of available plants for rental in the plant catalog using the plant name (a substring of it) and the rental period \n\n");

        System.out.println("Plant Inventory Items " + plantInventoryEntries);
        System.out.printf("\nServiceable Plant Inventory Items: #%s   ", plantInventoryItems.size());

        //Assertion
        assertThat(plantInventoryEntries.size()).isEqualTo(1);

        System.out.println("\n\nEnd... \n");

    }

    @Test
    public void queryTwo(){

        //Parameters
        LocalDate targetStart = LocalDate.of(2017, 03,22);
        LocalDate targetEnd = LocalDate.of(2017,03,24);


        //queries
        List<Long> plantInventoryItemUnavailableIds = plantReservationRepository.findPlantInventoryEntryIdWhereUnavailable(targetStart, targetEnd);
        List<Long> plantInventoryItemAvailable  = plantInventoryItemRepository.findAllIds();
        List<PlantInventoryItem> plantInventoryItems = plantInventoryItemRepository.findAllByIdWhereServiceable(plantInventoryItemAvailable, plantInventoryItemUnavailableIds);

        //Output
        System.out.printf("\nSecond Consultation: \n\nCheck there is at least one item for a given entry available for rental in a given period of time \n\n");

        System.out.printf("Available: %s \n", plantInventoryItems);
        System.out.printf("\nServiceable Plant Inventory Items: %s\n " ,plantInventoryItems.size());

        //Assertion
        assertThat(plantInventoryItems.size()).isEqualTo(1);

        System.out.println("\n\nEnd... \n");
    }

    @Test
    public void queryThree(){

        // Parameters
        LocalDate test = LocalDate.of(2017,02,10);      // Se simula que es el 2017.

        // Queries
        List<Long> allEntries = plantInventoryEntryRepository.findAllIds();                                         // Todas las Entries Menos -v
        List<Long> entriesUsed = plantReservationRepository.findPlantsUsedInPastSixMonths(test);                    // Las Entries que se usaron el periodo.
        List<PlantInventoryEntry> result = plantInventoryEntryRepository.findByYesNoIds(allEntries, entriesUsed);   // Entries de Resultado

        // Output
        System.out.printf("\nThird Consultation: \n\nThe list of plants that have not been hired during the last 6 months \n\n");

        System.out.printf("PlantInventoryEntries: %s\n", result);

        // Assertion
        assertThat(result.size()).isEqualTo(13);

        System.out.println("\n\nEnd... \n");
    }

    @Test
    public void queryFour() {

        // Parameters
        LocalDate targetEnd = LocalDate.now();
        LocalDate targetStart = targetEnd.minusYears(5);

        // Queries
        List<Pair> result = maintenanceTaskRepository.findRepairsByYear(targetStart, targetEnd);

        // Output
        System.out.printf("\nFourth Consultation: \n\nThe number of corrective repairs per year for the last 5 years \n\n");

        result.stream().forEach(System.out::println);

        System.out.println("\n\nEnd... \n");

    }

    @Test
    public void queryFive(){
        LocalDate targetEnd = LocalDate.now();
        LocalDate targetStart = targetEnd.minusYears(5);

        List<Pair> result = maintenanceTaskRepository.findRepairsByPrice(targetStart, targetEnd);

        // Output
        System.out.printf("\nFifth Consultation: \n\nThe cost due to corrective repairs per year for the last 5 years \n\n");

        result.stream().forEach(System.out::println);

        System.out.println("\n\nEnd... \n");
    }

}
